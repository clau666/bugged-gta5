﻿mp.events.add('playerEnterVehicle', (vehicle, seat) => {
    if (vehicle !== null && vehicle.hasVariable("pizzaJobCar") && vehicle.getVariable("pizzaJobCar") && seat == 0) {
        switch (seat) {
            case 1:
                mp.players.local.taskLeaveVehicle(vehicle.handle, 0);
                break;
            case 2:
                mp.players.local.taskLeaveVehicle(vehicle.handle, 0);
                break;
        }

        mp.gui.chat.push(`Type /pizza to start working.`);
        return;
    }
});

mp.events.add('playerExitVehicle', (player, vehicle) => {
    console.log(`when out from vehicle`);
});

var TargetsToRender = [];
var localPlayer = mp.players.local;

function CreateModel(model, pos, rot) {
    if (!mp.game.streaming.hasModelLoaded(mp.game.joaat(model)))
        mp.game.streaming.requestModel(mp.game.joaat(model));
    while (!mp.game.streaming.hasModelLoaded(mp.game.joaat(model)))
        mp.game.wait(0);
    return mp.objects.new(mp.game.joaat(model), pos,
        {
            rotation: rot,
            alpha: 255,
            dimension: mp.players.local.dimension
        });
}

function getboneindex(handle, boneid) {
    return mp.game.invoke("0x3F428D08BE5AAE31", handle, boneid);
}

mp.events.add('test', (player) => {
    if (player != localPlayer) {
        var position = localPlayer.getBoneCoords(28422, 0, 0, 0);
        TargetsToRender[localPlayer.remoteId] = CreateModel("ng_proc_pizza01a", position, localPlayer.getRotation(2));
        TargetsToRender[localPlayer.remoteId].attachTo(localPlayer.handle, getboneindex(28422), 0, 0.5, 0, 0, 0, 180, false, false, false, true, 2, true);

        mp.game.streaming.requestAnimDict("anim@heists@box_carry@");
        mp.players.local.taskPlayAnim("anim@heists@box_carry@", "idle", 4.0, 49, -1, 50, 0.0, true, true, true);
    }

    mp.gui.chat.push("test clienside");
});

let ped = mp.peds.new(
    mp.game.joaat('s_m_m_linecook'),
    new mp.Vector3(-1182.634, -883.8701, 13.800),
    310.0,
    mp.players.local.dimension
);













var housePositions = [new mp.Vector3(-1164.153, -347.823, 36.6606), new mp.Vector3(-1075.617, -1026.05, 4.545348), new mp.Vector3(-1062.696, -1054.296, 2.150357),
new mp.Vector3(-1029.947, -1107.495, 2.15897), new mp.Vector3(-1224.982, -1208.111, 8.26718), new mp.Vector3(-1282.826, -1252.504, 4.055181),
new mp.Vector3(-1043.678, -1022.894, 2.150353)];


var defaultPosition = housePositions[0];

var houseBlip = undefined;
var houseMarker = undefined;

setInterval(() => {
    if (mp.players.local.hasVariable("inPizzaJob") && mp.players.local.getVariable("inPizzaJob")) {
        if (mp.players.local.getVariable("setPizza") >= 5) {
            mp.gui.chat.push("playerStartedPizzaBoy");

            var randomNumber = Math.floor(Math.random() * 6);
            switch (randomNumber) {
                case 1:
                    defaultPosition = housePositions[1];
                    mp.gui.chat.push("1");
                    break;
                case 2:
                    defaultPosition = housePositions[2];
                    mp.gui.chat.push("2");
                    break;
                case 3:
                    defaultPosition = housePositions[3];
                    mp.gui.chat.push("3");
                    break;
                case 4:
                    defaultPosition = housePositions[4];
                    mp.gui.chat.push("4");
                    break;
                case 5:
                    defaultPosition = housePositions[5];
                    mp.gui.chat.push("5");
                    break;
                case 6:
                    defaultPosition = housePositions[6];
                    mp.gui.chat.push("6");
                    break;
            }
        }

        if (houseBlip == undefined && houseMarker == undefined) {
            houseBlip = mp.blips.new(1, defaultPosition, {
                name: 'Deliver Pizza',
                color: 1,
                shortRange: false,
            });

            houseMarker = mp.markers.new(1, new mp.Vector3(defaultPosition.x, defaultPosition.y, defaultPosition.z), 4, {
                direction: new mp.Vector3(0, 0, 0),
                rotation: new mp.Vector3(0, 0, 0),
                color: [255, 0, 0, 100],
                visible: true,
                dimension: 0
            });
        } else {
            if (houseBlip !== undefined) {
                houseBlip.destroy();
                houseBlip = undefined;

                houseMarker.destroy();
                houseMarker = undefined;
            }
        }
    } else {
        if (houseBlip !== undefined) {
            houseBlip.destroy();
            houseBlip = undefined;

            houseMarker.destroy();
            houseMarker = undefined;
        }
    }
}, 10000);
