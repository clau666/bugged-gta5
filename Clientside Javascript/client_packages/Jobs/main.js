var farmerColshape = undefined;
var farmerMarker = undefined;
var secElapsed = 20;
const localVehicle = mp.players.local.vehicle;
var farmerBlip = undefined;

mp.events.add('playerStartedFarmer', () => {
    farmerColshape = mp.colshapes.newSphere(1257.076, 1877.021, 78.87789, 108);
    farmerMarker = mp.markers.new(1, new mp.Vector3(1257.076, 1877.021, 50.87789), 210,
        {
            direction: new mp.Vector3(0, 0, 0),
            rotation: new mp.Vector3(0, 0, 0),
            color: [255, 0, 0, 40],
            visible: true,
            dimension: 0
        });

    secElapsed = 20;
});

mp.events.add('playerStopFarmer', () => {
    if (farmerColshape)
        farmerColshape.destroy();
    farmerColshape = undefined;

    if (farmerMarker)
        farmerMarker.destroy();
    farmerMarker = undefined;

    secElapsed = 20;

    if (mp.players.local.vehicle)
        mp.players.local.taskLeaveVehicle(mp.players.local.vehicle.handle, 0);
});

setInterval(function () {
    if (mp.players.local.hasVariable("inJob") && mp.players.local.getVariable("inJob") && mp.players.local.vehicle !== null && mp.players.local.vehicle.hasVariable("jobcar") && mp.players.local.vehicle.getVariable("jobcar")) {
        let speed = mp.players.local.vehicle.getSpeed();

        speed *= 3.6;
        if (speed < 10)
            mp.gui.chat.push("Conduce cu cel putin 10km ");
        else
            secElapsed -= 1;

        if (secElapsed <= 0) {
            mp.events.call("playerStopFarmer");
            mp.events.callRemote("playerFinishedFarmer");
        }
    }
}, 1000);

mp.events.add('playerEnterColshape', (shape) => {
    if (shape == farmerColshape)
        mp.game.graphics.notify(`You entered a colshape with id "${shape.id}".`);
});

mp.events.add('playerExitColshape', (shape) => {
    if (shape == farmerColshape) {
        mp.game.graphics.notify(`You left a colshape with id "${shape.id}".`);

        if (mp.players.local.vehicle)
            mp.players.local.taskLeaveVehicle(mp.players.local.vehicle.handle, 0);
    }
});

mp.events.addDataHandler('job', function (entity, value) {
    if (entity.type === 'player') {
        if (value == "farmer") {
            farmerBlip = mp.blips.new(1, new mp.Vector3(1219.8, 1817.814, 78.99397),
                {
                    name: 'Farmer Job',
                    color: 1,
                    shortRange: true,
                });
        } else {
            if (farmerBlip)
                farmerBlip.destroy();
            farmerBlip = undefined;
        }
    }
});

mp.events.add('destroyFarmer', () => {
    if (farmerBlip)
        farmerBlip.destroy();
    farmerBlip = undefined;
});

mp.events.add('render', () => {
    if (mp.players.local.hasVariable("inJob") && mp.players.local.getVariable("inJob")) {
        var color = [255, 255, 255, 255];

        if (mp.players.local.vehicle !== null && mp.players.local.vehicle.hasVariable("jobcar") && mp.players.local.vehicle.getVariable("jobcar")) {
            let speed = mp.players.local.vehicle.getSpeed();

            speed *= 3.6;
            if (speed < 10)
                color = [255, 0, 0, 255];
        }

        mp.game.graphics.drawText(`seconds: ${secElapsed}`, [0.5, 0.005],
            {
                font: 7,
                color: color,
                scale: [0.5, 0.5],
                outline: true
            });
    }
});

let ped = mp.peds.new(
    mp.game.joaat('a_m_m_farmer_01'),
    new mp.Vector3(1219.42, 1847.864, 79),
    180.0,
    mp.players.local.dimension
);

let ped2 = mp.peds.new(
    mp.game.joaat('s_m_y_airworker'),
    new mp.Vector3(1219.8, 1817.814, 79.100),
    360.0,
    mp.players.local.dimension
);