var armsDealerBlip = undefined;
var getMatsBlip = undefined;
var getMatsColshape = undefined;
var armsDealerColshape = undefined;
var armsDealerMarker = undefined;

mp.events.addDataHandler('job', function (entity, value) {
    if (entity.type === 'player') {
        if (value == "Arms Dealer") {
            armsDealerBlip = mp.blips.new(1, new mp.Vector3(188.6136, 348.1722, 107.6055), {
                name: 'Collect Materials',
                color: 5,
                shortRange: true,
            });

            getMatsColshape = mp.colshapes.newSphere(188.6136, 348.1722, 107.6055, 4);
        } else {
            if (armsDealerBlip)
                armsDealerBlip.destroy();
            armsDealerBlip = undefined;

            if (getMatsBlip)
                getMatsBlip.destroy();
            getMatsBlip = undefined;

        }
    }
});

mp.events.add('Job:addCheckpointToMats', () => {
    mp.gui.chat.push("!{#F0ACAC}Go to the checkpoint to collect the materials.");

    if (armsDealerBlip)
        armsDealerBlip.destroy();
    armsDealerBlip = undefined;

    getMatsBlip = mp.blips.new(1, new mp.Vector3(1514.142, 1704.595, 110.2635), {
        name: 'Collect Materials',
        color: 5,
        shortRange: false,
    });

    armsDealerColshape = mp.colshapes.newSphere(1514.142, 1704.595, 109.2635, 4);
    armsDealerMarker = mp.markers.new(1, new mp.Vector3(1514.142, 1704.595, 109.2635), 4, {
        direction: new mp.Vector3(0, 0, 0),
        rotation: new mp.Vector3(0, 0, 0),
        color: [255, 0, 0, 100],
        visible: true,
        dimension: 0
    });
});

mp.events.add('playerEnterColshape', (shape) => {
    if (shape == armsDealerColshape) {
        if (armsDealerMarker)
            armsDealerMarker.destroy();
        armsDealerMarker = undefined;

        if (getMatsBlip)
            getMatsBlip.destroy();
        getMatsBlip = undefined;

        if (armsDealerColshape)
            armsDealerColshape.destroy();
        armsDealerColshape = undefined;

        armsDealerBlip = mp.blips.new(1, new mp.Vector3(188.6136, 348.1722, 107.6055), {
            name: 'Collect Materials',
            color: 5,
            shortRange: true,
        });

        mp.gui.chat.push(`!{#CEF0AC}You collected 1,000 materials.`);
        mp.events.callRemote("finishedDelieveringMats");
    }
});

let ped = mp.peds.new(
    mp.game.joaat('s_m_y_dealer_01'),
    new mp.Vector3(244.1408, -42.49718, 69.89651),
    110.0,
    mp.players.local.dimension
);