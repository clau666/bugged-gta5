mp.events.add('render', () => {
    mp.game.ui.hideHudComponentThisFrame(3); // hide cash

    mp.game.graphics.drawText(`casperrpg.ro`, [0.952, 0.973],
        {
            font: 7,
            color: [255, 255, 255, 255],
            scale: [0.5, 0.5],
            outline: true
        });

        mp.game.graphics.drawText(`~r~${mp.players.local.name}`, [0.980, 0.950],
        {
            font: 7,
            color: [255, 255, 255, 255],
            scale: [0.5, 0.5],
            outline: true
        });

        var date = new Date();
        var hours = (date.getHours() < 10? '0' : '') + date.getHours();
        var minutes = (date.getMinutes() < 10? '0' : '') + date.getMinutes();
        var month = (date.getMonth() + 1 < 10? '0' : '') + date.getMonth();

        mp.game.graphics.drawText(`~w~` + `${date.getDate() + `.` + (month) + `.` + date.getFullYear()}`, [0.925, 0.03],
        {
            font: 7,
            color: [255, 255, 255, 255],
            scale: [0.43, 0.43],
            outline: true
        });

        mp.game.graphics.drawText(`~w~` + `${hours + `:` + (minutes)}`, [0.925, 0.047],
        {
            font: 7,
            color: [255, 255, 255, 255],
            scale: [0.85, 0.85],
            outline: true
        });
})