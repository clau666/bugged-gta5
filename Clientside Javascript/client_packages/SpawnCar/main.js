var lastTime = undefined;

mp.keys.bind(0x32, true, function () {
  if (lastTime !== undefined && Math.abs((new Date().getTime() - lastTime.getTime()) / 1000) < 2)
    return;

  mp.events.callRemote('StartEngineOn2');
  lastTime = new Date();
});

mp.events.add("playerEnterVehicle", function (vehicle, seat) {
  if (vehicle.hasVariable("engineOn") && !vehicle.getVariable("engineOn"))
    vehicle.setEngineOn(false, true, true);
});