var current_checkpoint = undefined;
var current_blip = undefined;
var current_marker = undefined;

mp.events.add("MarksToPlayer:createCheckpoint", function(model, pos, dir, scale, color, dimension) {
    current_checkpoint = mp.checkpoints.new(model, pos, {
        direction: dir,
        color: color,
        visible: true,
        radius: scale,
        dimension: dimension
    });
});

mp.events.add("MarksToPlayer:createMarker", function(sprite, position, scale, color, name, alpha, drawDistance, shortRange, rotation, dimension) {
    current_marker = mp.markers.new(sprite, position, scale, {
        direction: direction,
        rotation: rotation,
        color: color,
        visible: visible,
        dimension: dimension
    });
});

mp.events.add("MarksToPlayer:createBlip", function(sprite, position, scale, color, name, alpha, drawDistance, shortRange, rotation, dimension) {
    current_blip = mp.blips.new(sprite, position, {
        name: name,
        scale: scale,
        color: color,
        alpha: alpha,
        drawDistance: drawDistance,
        shortRange: shortRange,
        rotation: rotation,
        dimension: dimension
    });
});

mp.events.add("MarksToPlayer:destroyCheckpoint", function(sprite, position, scale, color, name, alpha, drawDistance, shortRange, rotation, dimension) {
    if (current_checkpoint) {
        current_checkpoint.destroy();
        current_checkpoint = undefined;
    }
});

mp.events.add("MarksToPlayer:destroyBlip", function(sprite, position, scale, color, name, alpha, drawDistance, shortRange, rotation, dimension) {
    if (current_blip) {
        current_blip.destroy();
        current_blip = undefined;
    }
});

mp.events.add("MarksToPlayer:destroyMarker", function(sprite, position, scale, color, name, alpha, drawDistance, shortRange, rotation, dimension) {
    if (current_marker) {
        current_marker.destroy();
        current_marker = undefined;
    }
});