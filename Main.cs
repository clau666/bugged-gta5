﻿using GTANetworkAPI;

namespace stunt {
    class Main : Script {
        [ServerEvent( Event.ResourceStart )]
        public void OnResourceStart( ) {
            NAPI.Server.SetGlobalServerChat( false );
            NAPI.Server.SetCommandErrorMessage( "!{#A9C4E4}SERVER: ~w~Unknown command." );
        }
    }
}
