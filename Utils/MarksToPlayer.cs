﻿using System;
using System.Collections.Generic;
using System.Text;
using GTANetworkAPI;

namespace stunt.Utils
{
    public class MarksToPlayer
    {
        public void CreateCheckpoint( Player player, uint model, Vector3 pos, Vector3 dir, float scale, Color color, uint dimension = 0 )
        {
            player.TriggerEvent( "MarksToPlayer:CreateCheckpoint", model, pos, dir, scale, color, dimension );
        }

        public void CreateBlip( Player player, uint sprite, Vector3 position, float scale, byte color, string name = "", byte alpha = 255, float drawDistance = 0, bool shortRange = false, short rotation = 0, uint dimension = 0 )
        {
      
            player.TriggerEvent( "MarksToPlayer:CreateBlip", sprite, position, scale, color, name, alpha, drawDistance, shortRange, rotation, dimension );
        }

        public void CreateMarker( Player player, uint markerType, Vector3 pos, Vector3 dir, Vector3 rot, float scale, Color color, bool bobUpAndDown = false, uint dimension = uint.MaxValue )
        {
            player.TriggerEvent( "MarksToPlayer:CreateMarker", markerType, pos, dir, scale, rot, color, bobUpAndDown, dimension );
        }
        public void DestroyCheckpoint( Player player )
        {
            player.TriggerEvent( "MarksToPlayer:DestroyCheckpoint" );

        }
        public void DestroyMarker( Player player )
        {
            player.TriggerEvent( "MarksToPlayer:DestroyMarker" );

        }
        public void DestroyBlip( Player player )
        {
            player.TriggerEvent( "MarksToPlayer:DestroyBlip" );
        }
    }
}
