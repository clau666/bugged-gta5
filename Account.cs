using GTANetworkAPI;
using stunt;
using System;
using System.Data.Common;
using System.Threading.Tasks;

namespace Account {

    public class Controller {
        public String username { get; set; }
        public String password { get; set; }
        public Player player { get; set; }
        public int admin   // property
        {
            get { return player.GetSharedData<int>( "admin" ); }
            set { player.SetSharedData( "admin", value ); }
        }
        public int money   // property
        {
            get { return player.GetSharedData<int>( "money" ); }
            set { player.SetSharedData( "money", value ); }
        }

        public int level   // property
        {
            get { return player.GetSharedData<int>( "level" ); }
            set { player.SetSharedData( "level", value ); }
        }
        public int sqlid   // property
        {
            get { return player.GetSharedData<int>( "sqlid" ); }
            set { player.SetSharedData( "sqlid", value ); }
        }
        public string job   // property
        {
            get { return player.GetSharedData<string>( "job" ); }
            set { player.SetSharedData( "job", value ); }
        }

        public int materials   // property
        {
            get { return player.GetSharedData<int>( "materials" ); }
            set { player.SetSharedData( "materials", value ); }
        }

        public Controller( ) {
        }

        public Controller( Player client ) {
            this.player = client;
        }
    }

    public class DBAccount {
        public static async Task LoadPlayerInformationAsync( Player player, string username ) {
            Controller controller = new Controller( player );
            await DBManager.SelectQueryAsync( $"SELECT (*) FROM accounts WHERE username = @username LIMIT 1", ( DbDataReader reader ) => {

                if ( reader.HasRows ) {
                    controller.level = ( int ) reader[ "level" ];
                    controller.username = username;
                    controller.job = ( string ) reader[ "job" ];
                    controller.materials = ( int ) reader[ "materials" ];
                    controller.admin = ( int ) reader[ "admin" ];
                    controller.money = ( int ) reader[ "money" ];
                    controller.sqlid = ( int ) reader[ "id" ];

                }
            } ).Result.AddValue( "@username", username ).Execute( );
        }
    }
}