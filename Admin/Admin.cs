﻿using GTANetworkAPI;

namespace stunt.Admin {
    public class Admin : Script {
        public void SendMessageToAdmins( string message ) {
            NAPI.Pools.GetAllPlayers( ).FindAll( player => GetAdmin( player ) > 0 ).ForEach( player => { player.SendChatMessage( message ); } );
        }

        public int GetAdmin( Player player ) {
            if ( player.HasSharedData( "admin" ) )
                return player.GetSharedData<int>( "admin" );

            return 0;
        }

        [Command( "gotocoords", "Syntax: /gotocoords [x] [y] [z]" )]
        public void GotoCoords( Player player, float x, float y, float z ) {
            if ( GetAdmin( player ) > 0 ) {
                player.Position = new Vector3( x, y, z );

                SendMessageToAdmins( $"Admin {player.Name} used /gotocoords." );
            } else
                player.SendChatMessage( "!{#CEF0AC}You need to be an admin." );
        }

        [Command( "aaa2" )]
        public void aaa2( Player player ) {
            if ( GetAdmin( player ) > 0 ) {
                player.Position = new Vector3( -993.296, -3147.131, 13.944 );
                player.Dimension = 1337;
                player.SendChatMessage( $"Welcome to LS International Airport (in virtual world {player.Dimension})." );

                SendMessageToAdmins( $"Admin {player.Name} used /aaa2." );
            } else
                player.SendChatMessage( "!{#CEF0AC}You need to be an admin." );
        }

        [Command( "gotoa" )]
        public void GotoA( Player player ) {
            if ( GetAdmin( player ) > 0 ) {
                player.Position = new Vector3( -993.296, -3147.131, 13.944 );
                player.Dimension = 0;
                player.SendChatMessage( $"Welcome to LS International Airport (in virtual world {player.Dimension})." );
            } else
                player.SendChatMessage( "!{#CEF0AC}You need to be an admin." );
        }

        [Command( "coords" )]
        public void GetCoords( Player player ) {
            player.SendChatMessage( $"Coords: {player.Position.X}, {player.Position.Y}, {player.Position.Z} | VW: {player.Dimension}" );
        }

        [Command( "mark" )]
        public void MarkPosition( Player player ) {
            if ( GetAdmin( player ) > 0 ) {
                player.SetData( "lastPos", player.Position );
                player.SendChatMessage( "Saved your last position." );
            } else
                player.SendChatMessage( "!{#CEF0AC}You need to be an admin." );
        }

        [Command( "gotomark" )]
        public void GotoMark( Player player ) {
            if ( GetAdmin( player ) > 0 ) {
                if ( player.HasData( "lastPos" ) ) {
                    player.Position = player.GetData<Vector3>( "lastPos" );
                }
            } else
                player.SendChatMessage( "!{#CEF0AC}You need to be an admin." );
        }
    }
}