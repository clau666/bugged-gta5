﻿using System;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;
using BCrypt;
using GTANetworkAPI;
using Account;
namespace stunt {
    public class  LoginSystem : Script {
        [ServerEvent( Event.ResourceStart )]
        public void ResourceStart( ) {
            Encoding.RegisterProvider( CodePagesEncodingProvider.Instance );
        }

        public static async Task<bool> LoginPlayer( Player player, string username, string password ) {
            bool exist = false;
            await DBManager.SelectQueryAsync( $"SELECT username FROM accounts WHERE username = @username AND password = @password LIMIT 1", ( DbDataReader reader ) => {
                exist = reader.HasRows;
            } ).Result.AddValue( "@username", username ).AddValue( "@password", password ).Execute( ) ;
            return exist;
        }

        public static async Task<bool> RegisterPlayer( string username, string password ) {
            bool exist = false;

            await DBManager.SelectQueryAsync( $"SELECT * FROM accounts WHERE username = @username", ( DbDataReader reader ) => {
                exist = reader.HasRows;
            } ).Result.AddValue( "@username", username ).Execute( ) ;

            if ( !exist ) {
                await DBManager.UpdateQueryAsync( $"INSERT INTO accounts (username, password) VALUES (@username, @password" ).Result.AddValue( "@username", username ).AddValue( "@password", password ).Execute( );
            } else
                Console.WriteLine( "[Register Attempt]" );

            return exist;
        }

        [RemoteEvent( "OnPlayerLoginAttempt" )]
        public async Task OnPlayerLoginAttempt( Player player, string username, string password ) {
            NAPI.Util.ConsoleOutput( $"[Login] Player {username} has joined." );

            if ( await LoginPlayer(player, username, password ) ) {
                player.TriggerEvent( "LoginResult", 1 );
                player.SetData( "logged", true );
                await DBAccount.LoadPlayerInformationAsync( player, username );
                
                player.Name = username;
                player.SetSkin( 0xF00B49DB );
            } else {
                player.TriggerEvent( "LoginResult", 0 );
            }
        }

        [RemoteEvent( "OnPlayerRegisterAttempt" )]
        public async Task OnPlayerRegisterAttempt( Player player, string username, string password ) {
            NAPI.Util.ConsoleOutput( $"[Register Attempt] Username {username} | Password: {password}" );

            if ( await RegisterPlayer( username, password ) ) {
                player.TriggerEvent( "RegisterResult", 0 );
            } else {
                player.TriggerEvent( "RegisterResult", 1 );
                player.SetData( "logged", true );
                player.Name = username;
            }
        }
    }
}
