﻿using GTANetworkAPI;
using System;

namespace stunt {
    class SpawnCar : Script {
        [Command( "spawncar" )]
        public void spawncar( Player player, string carType, int primaryColor = 88, int secondaryColor = 88, string licensePlate = "") {
            if ( !player.HasData( "logged" ) || !player.GetData<bool>("logged")) {
                player.SendChatMessage( "You are not logged." );
                return;
            }

            Vehicle veh = NAPI.Vehicle.CreateVehicle( NAPI.Util.GetHashKey( carType ), player.Position.Around( 5 ), 0.0f, primaryColor, secondaryColor, licensePlate, 255, false, false );
            veh.NumberPlate = licensePlate;
            veh.SetSharedData( "engineOn", false );
            player.SendChatMessage( $"You spawned a(n) {carType}." );
        }

        [Command("engine", "Syntax [/engine].")]
        public void StartEngine( Player player ) {
            if ( !player.HasData( "logged" ) || !player.GetData<bool>( "logged" ) ) {
                player.SendChatMessage( "You are not logged." );
                return;
            }

            if ( player.IsInVehicle && player.VehicleSeat == -1 ) {
                player.Vehicle.EngineStatus = !player.Vehicle.EngineStatus;
                if ( player.Vehicle.EngineStatus ) {
                    player.Vehicle.EngineStatus = true;
                    foreach ( Player players in NAPI.Player.GetPlayersInRadiusOfPlayer( 30, player ) )
                        player.SendChatMessage( "!{#afa1c4}* " + player.Name + $"[{player.Value}] started the engine of his { player.Vehicle.DisplayName}." );
                } else {
                    player.Vehicle.EngineStatus = false;
                    foreach ( Player players in NAPI.Player.GetPlayersInRadiusOfPlayer( 30, player ) )
                        player.SendChatMessage( "!{#afa1c4}* " + player.Name + $"[{player.Value}] stopped the engine of his { player.Vehicle.DisplayName}." );
                }
            }
        }

        [RemoteEvent( "StartEngineOn2" )]
        public void StartEngineOn2( Player player ) {
            if ( player.IsInVehicle && !player.Vehicle.IsNull && player.VehicleSeat == -1 ) {
                player.Vehicle.EngineStatus = !player.Vehicle.EngineStatus;

                if ( player.Vehicle.EngineStatus ) {
                    player.Vehicle.SetSharedData( "engineOn", true );
                    foreach ( Player players in NAPI.Player.GetPlayersInRadiusOfPlayer( 30, player ) ) {
                        player.SendChatMessage( "!{#afa1c4}* " + player.Name + $"[{player.Value}] started the engine of his { player.Vehicle.DisplayName}." );
                    }
                } else {
                    player.Vehicle.SetSharedData( "engineOn", false );
                    foreach ( Player players in NAPI.Player.GetPlayersInRadiusOfPlayer( 30, player ) )
                        player.SendChatMessage( "!{#afa1c4}* " + player.Name + $"[{player.Value}] stopped the engine of his { player.Vehicle.DisplayName}." );
                }
            }
        }
    }
}