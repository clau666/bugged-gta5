﻿using System;
using System.Data.Common;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace stunt.Database
{
    /// <summary>
    /// Query base class
    /// </summary>
    abstract class Query
    {
        private String queryString;
        protected Action<DbDataReader> code;
        protected MySqlCommand mysqlCommand;
        protected MySqlConnection connection;

        public Query(String query, Action<DbDataReader> code = null)
        {
            this.queryString = query;
            this.code = code;
            this.connection = DBManager.Instance().GetNewConnection();
            mysqlCommand = new MySqlCommand(query, this.connection);

        }

        public Query AddValue( string key, object value ) {
            mysqlCommand.Parameters.AddWithValue( key, value );
            return this;
        }

        abstract public  Task<int> Execute();
    }
}
