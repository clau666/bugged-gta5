﻿using System;
using System.Data.Common;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace stunt.Database
{
    /// <summary>
    /// SQL Insert, UPDATE, DELETE query wrapper
    /// </summary>
    class NormalQuery : Query
    {
        public NormalQuery(string query, Action<DbDataReader> code = null) : base(query, code){ }

        public override async Task<int> Execute()
        {
            int id = 0;
            try
            {
                await mysqlCommand.ExecuteNonQueryAsync().ConfigureAwait(false);
                connection.Close();
                id =  (int)mysqlCommand.LastInsertedId;
            }
            catch(Exception e)
            {
                Console.WriteLine($"[DEBUG] MYSQL ERROR AT {DateTime.Now.ToString()} : {e.ToString()}");
            }
            return id;
        }
    }
}
