﻿using System;
using System.Collections.Generic;
using System.Text;

namespace stunt.Singleton {
    public abstract class Singleton<T>
    where T : Singleton<T>, new() {
        private static T _instance = new T( );
        public static T Instance( ) {
            return _instance;
        }
    }
}