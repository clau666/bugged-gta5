﻿using System;
using System.Data.Common;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace stunt.Database
{
   
    class SelectQuery : Query
    {
        public SelectQuery(string query, Action<DbDataReader> code) : base(query, code) { }

        public override async Task<int> Execute()
        {
            DbDataReader reader = await mysqlCommand.ExecuteReaderAsync().ConfigureAwait(false);

            if (reader.HasRows)
            {
                 while( await reader.ReadAsync().ConfigureAwait(false))
                {
                    code(reader);
                }
            }
          
            reader.Close();
            connection.Close();
            return 1;
        }
    }
}
