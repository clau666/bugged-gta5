﻿using System;
using System.Data.Common;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using stunt.Database;
using stunt.Singleton;

namespace stunt
{
    /// <summary>
    /// Class for handling all database interactions such as connecting and queries.
    /// </summary>
    class DBManager : Singleton<DBManager>
    {
        public DBManager() { }

        public string Password { get; set; }
        private MySqlConnection connection = null;
        public MySqlConnection Connection
        {
            get { return connection; }
        }

     
        public static MySqlCommand SimpleQuery(String query)
        {
            DBManager.Instance().connection = DBManager.Instance().GetNewConnection();
            return new MySqlCommand(query, DBManager.Instance().connection);
        }


        public static async Task<bool> IsTableEmptyAsync(String tableName)
        {
            bool isEmpty = true;
            string queryString = "SELECT * FROM " + tableName;
            await DBManager.SelectQueryAsync(queryString, (DbDataReader reader) =>
             {
                 isEmpty = !reader.HasRows;
             }).Result.Execute();
            return isEmpty;
        }

        
     
        public static async Task<SelectQuery> SelectQueryAsync(String query, Action<DbDataReader> code)
        {
            return new SelectQuery(query, code);
        }

        public static async Task<Query> UpdateQueryAsync(String query)
        {
            return new NormalQuery(query);
        }



        private String LoadConnectionInfo( ) {
            string host = "127.0.0.1";
            string database = "sticks_db";
            string user = "root";
            string password = "";
            return String.Format( "Server={0}; database={1}; UID={2}; password={3}", host, database, user, password );
        }


        public MySqlConnection GetNewConnection()
        {
           var con = new MySqlConnection(LoadConnectionInfo());
            con.Open();

            return con;
        }

        public void Close()
        {
            connection.Close();
        }
    }
}
