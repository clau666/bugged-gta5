﻿using GTANetworkAPI;
using System;
using System.Threading.Tasks;


namespace stunt.Jobs {
    class Farmer : Script {
        
        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart() {
            NAPI.TextLabel.CreateTextLabel( "~w~Job: ~r~Farmer\n~w~Use ~r~/getjob ~w~to get this job.", new Vector3( 1219.42, 1847.864, 78.99397 ), 15, 1, 4, new Color( 255, 255, 255, 255 ) );
            NAPI.TextLabel.CreateTextLabel( "~w~Job: ~r~Farmer\n~w~Use ~r~/work ~w~to start working.", new Vector3( 1219.8, 1817.814, 78.99397 ), 15, 2, 4, new Color( 0, 162, 255, 255 ) );
            Job.joblist.Add(new Job.JobModel("farmer", new Vector3(1219.42, 1847.864, 78.99397), 2));
        }

        [Command( "work" )]
        public void StartWork( Player player ) {
            if ( Job.DoesPlayerHaveJob( player ) != "farmer" ) {
                player.SendChatMessage( "!{#2CB6D0}You are not a Farmer." );
                return;
            }

            if ( player.Position.DistanceTo( new Vector3( 1219.8, 1817.814, 78.99397 ) ) > 4 )
                return;

            Vehicle veh = NAPI.Vehicle.CreateVehicle( -2076478498, player.Position.Around( 5 ), 0.0f, 1, 1, "job", 255, true, false );
            Job.SetInJob( player, true );

            veh.SetSharedData( "engineOn", false );
            veh.SetSharedData( "jobcar", true );
            player.SetSharedData( "inFarmerWork", true );
            player.SetIntoVehicle( veh, -1 );
            player.TriggerEvent( "Job:playerStartedFarmer" );
            player.TriggerEvent( "Job:destroyFarmer" );
        }

        [RemoteEvent( "Job:playerFinishedFarmer" )]
        public void OnPlayerFinishedFarmer( Player player ) {
            Job.SetInJob( player, false );
            player.SendChatMessage( $"Ai vandut un sac de faina si ai primit un cacat " );
          
        }

        [ ServerEvent( Event.PlayerExitVehicleAttempt )]
        public void OnPlayerExitVehicle( Player player, Vehicle vehicle ) {
            if ( Job.DoesPlayerHaveJob(player) == "farmer" && vehicle.HasSharedData( "jobcar" ) && vehicle.GetSharedData<bool>( "jobcar" ) ) {
                vehicle.Delete( );
                if ( player.HasSharedData( "inJob" ) && player.GetSharedData<bool>( "inJob" ) ) {
                    player.SendChatMessage( "!{#2CB6D0}You job failed because you left the tractor." );
                    player.TriggerEvent( "Job:playerStopFarmer" );
                    player.SetSharedData( "inJob", false );
                }
            }
        }
    }
}
