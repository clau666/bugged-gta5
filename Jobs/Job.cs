﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GTANetworkAPI;
namespace stunt.Jobs
{
    public class Job : Script
    {

        public class JobModel
        {
            public int id { get; set; }
            public string name { get; set; }
            public Vector3 position { get; set; }
           public Vector3 temp_position { get; set; }
            public JobModel() { }
            public JobModel(string name, Vector3 position, int id)
            {
                this.name = name;
                this.position = position;
                this.id = id;
            }
        }

        public static List<JobModel> joblist = new List<JobModel>();
            
        public static bool IsInJob(Player player)
        {
            return player.HasSharedData( "inJob" ) ? player.GetSharedData<bool>( "inJob" ) : false;
        }

        public static void SetInJob( Player player, bool boolean )
        {
            player.SetSharedData( "inJob", boolean );
        }

        public static void SetJobType( Player player, string type ) {
            player.SetSharedData( "jobType", type );
        }

        public static string DoesPlayerHaveJob( Player player )
        {
            if (player.HasSharedData("job"))
                return player.GetSharedData<string>("job");

            return "none";
        }
        public JobModel GetClosestJob( Player player )
        {
            JobModel closest = new JobModel("none", new Vector3(), 0);
            if ( DoesPlayerHaveJob( player ) == "none" ) {
                float closest_distance = float.MaxValue;
                foreach ( var job in joblist ) {
                    float dist = player.Position.DistanceTo( job.position );

                    if ( closest_distance > dist ) {
                        closest_distance = dist;
                        closest = job;
                    }
                }
            }
            return closest;
        }

        [Command("quitjob")]
        public void QuitJob( Player player )
        {
            if (player.IsInVehicle)
            {
                player.SendChatMessage("!{#CEF0AC}Poti folosi aceasta comanda doar cand nu te afli intr-un vehicul.");
                return;
            }

            if ( DoesPlayerHaveJob( player) == "none")
            {
                player.SendChatMessage("!{#CECECE}You don't have a job.");
                return;
            }

            new Account.Controller( player ).job = "none";

            player.SendChatMessage("!{#CECECE}You have quit your job.");
            player.TriggerEvent( "MarksToPlayer:destroyBlip" );
        }

        [Command( "getjob" )]
        public void GetJob( Player player )
        {
            if ( player.IsInVehicle ) {
                player.SendChatMessage( "!{#CEF0AC}Poti folosi aceasta comanda doar cand nu te afli intr-un vehicul." );
                return;
            }

            if ( DoesPlayerHaveJob( player ) != "none" )
            {
                player.SendChatMessage( "!{#CECECE}You already have a job." );
                return;
            }

            var closest_job = GetClosestJob(player);
            if ( player.Position.DistanceTo( closest_job.position ) > 4 )
                return;
   

            new Account.Controller( player ).job = closest_job.name;

            player.SendChatMessage( $"{"!{#2CB6D0}"}Your job is now {closest_job.name}." );
            player.TriggerEvent( "MarksToPlayer:createBlip", 0, closest_job.temp_position);

        }

        [Command( "gotojob", "Syntax: /gotojob [id]" )]
        public void OnGotoJob( Player player, int id ) {
            player.Position = Job.joblist.Find( job => job.id == id ).position;
        }
    }
}
