﻿using GTANetworkAPI;
using System;
using System.Threading.Tasks;

namespace stunt.Jobs {
    public class ArmsDealer : Script {
        [ServerEvent( Event.ResourceStart )]
        public void OnResourceStart( ) {
            NAPI.TextLabel.CreateTextLabel( "~r~Arms Dealer\n~w~Use ~r~/getjob ~w~to get this job.", new Vector3( 244.1408, -42.49718, 69.89651 ), 15, 1, 4, new Color( 0, 162, 255, 255 ) );
            NAPI.TextLabel.CreateTextLabel( "Materials Pickup!\n\nType /getmats as an Arms Dealer\nto collect materials.", new Vector3( 188.6136, 348.1722, 107.6055 ), 15, 1, 4, new Color( 0, 162, 255, 255 ) );
            Job.joblist.Add( new Job.JobModel( "Arms Dealer", new Vector3( 244.1408, -42.49718, 69.89651 ), 1 ) );
        }

        [Command( "getmats" )]
        public void GetMats( Player player ) {
            if ( Job.DoesPlayerHaveJob( player ) != "Arms Dealer" ) {
                player.SendChatMessage( "!{#2CB6D0}You are not an Arms Dealer." );
                return;
            }

            if ( player.Position.DistanceTo( new Vector3( 188.6136, 348.1722, 107.6055 ) ) > 10 )
                return;

            player.TriggerEvent( "Job:addCheckpointToMats" );
            player.SetSharedData( "inArmsDealerWork", true );
        }

        [RemoteEvent( "finishedDelieveringMats" )]
        public async Task OnPlayerDelieveredMats( Player player ) {
            player.SetSharedData( "inArmsDealerWork", false );
        }

    }
}