using GTANetworkAPI;

namespace stunt.Jobs {
    public class PizzaBoy : Script {
        [ServerEvent( Event.ResourceStart )]
        public void OnResourceStart( ) {
            NAPI.TextLabel.CreateTextLabel( "~w~ID: ~r~3\n~w~Job: ~r~Pizza Boy\n~w~Use ~r~/getjob ~w~to get this job.", new Vector3( -1182.634, -883.8701, 13.800 ), 15, 1, 4, new Color( 0, 162, 255, 255 ) );
            //NAPI.TextLabel.CreateTextLabel( "Materials Pickup!\n\nType /getmats as an Arms Dealer\nto collect materials.", new Vector3( -1182.634, -883.8701, 13.76559 ), 15, 1, 4, new Color( 0, 162, 255, 255 ) );
            Job.joblist.Add( new Job.JobModel( "Pizza Boy", new Vector3( -1182.634, -883.8701, 15.76559 ), 3 ) );
        }

        [ Command( "s" )]
        public void s( Player player ) {
            Vehicle vehicle = NAPI.Vehicle.CreateVehicle( 0x9229E4EB, new Vector3( -1173.58, -872.4163, 14.16597 ), 90, 0, 0 );
            vehicle.SetSharedData( "engineOn", false );
            vehicle.SetSharedData( "pizzaJobCar", true );

            Vehicle vehicle1 = NAPI.Vehicle.CreateVehicle( 0x9229E4EB, new Vector3( -1171.454, -875.7386, 14.16864 ), 90, 0, 0 );
            vehicle1.SetSharedData( "engineOn", false );
            vehicle1.SetSharedData( "pizzaJobCar", true );

            Vehicle vehicle2 = NAPI.Vehicle.CreateVehicle( 0x9229E4EB, new Vector3( -1169.904, -879.319, 14.1532 ), 90, 0, 0 );
            vehicle2.SetSharedData( "engineOn", false );
            vehicle2.SetSharedData( "pizzaJobCar", true );
        }

        [Command( "pizza" )]
        public void GetPizza( Player player ) {
            if ( player.Position.DistanceTo( new Vector3( -1173.805, -886.7996, 13.9418 ) ) > 20 ) {
                player.SendChatMessage( "!{#CECECE}You can use this command while you are nearby the Pizza Boy job." );
                return;
            }

            if ( Job.DoesPlayerHaveJob( player ) != "Pizza Boy" ) {
                player.SendChatMessage( "!{#CECECE}You don't have the pizza job." );
                return;
            }
            
            if ( !player.IsInVehicle ) {
                player.SendChatMessage( "!{#CECECE}You can only use this command while you are in a pizza scooter." );
                return;
            }

            player.SetSharedData( "inPizzaJob", true );
            player.SetSharedData( "setPizza", 5 );

           // player.TriggerEvent( "Job:playerStartedPizzaBoy" );
        }
    }
}